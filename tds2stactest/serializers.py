# SPDX-FileCopyrightText: 2023 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

from rest_framework import serializers

from .models import APIHarvesterIngester


class APIHarvesterIngesterSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIHarvesterIngester
        fields = "__all__"  # You can also specify specific fields if needed


class APIHarvesterIngesterSelectedSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIHarvesterIngester
        fields = (
            "id",
            "engine",
            "job_name",
            "email",
            "created_at",
            "harvesting_datetime",
            "ingesting_datetime",
            "harvesting_status",
            "ingesting_status",
            "general_status",
        )


class SaveCloseSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIHarvesterIngester
        fields = "__all__"  # You can also specify specific fields if needed


class APIHarvesterIngesterDepthClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIHarvesterIngester
        fields = (
            "id",
            "job_name",
            "email",
            "depth_number",
            "client_collection_ids",
            "client_collection_titles",
            "client_collection_descriptions",
        )
