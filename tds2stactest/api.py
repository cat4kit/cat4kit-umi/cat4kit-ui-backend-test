# SPDX-FileCopyrightText: 2023 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0


import json
import os
import shutil
import traceback

# from .serializers import APIHarvesterSerializer
from datetime import datetime

import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

# from django.shortcuts import get_list_or_404
from insupdel4stac import InsUpDel4STAC
from rest_framework import status

# from logger.models import Logger
# from logger.serializers import LoggerNameSerializer, LoggerSerializer
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from tds2stac import (
    NestedCollectionInspector,
    Recognizer,
    TDS2STACIntegrator,
    WebServiceListScraper,
)

from .forms import (
    HarvesterRecognizerForm,
    InitialRecognizingForm,
    NestedCollectionForm,
    SaveCloseForm,
    SaveHarvestIngestCloseForm,
    SaveUpdateCloseForm,
)
from .models import APIHarvesterIngester
from .serializers import (
    APIHarvesterIngesterDepthClientSerializer,
    APIHarvesterIngesterSelectedSerializer,
    APIHarvesterIngesterSerializer,
)

# TODO: tds2stac_updating_by_id: It contains TDS2STACIntegrator and works in the editor of the object based on the ID of model that is created by Test Job or Scheduler Job.


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_recognizing_initial_trigger(request):
    """
    This is a function for running the Recognizer and Nested_Collection functions
    of tds2stac.
    """

    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Making labels for the fields that are not filled by the user
    manual_req = {
        "created_at": datetime.now(),
        "depth_number_max": 10,
        "general_status": "pending",
        "harvesting_datetime": None,
        "harvesting_status": "pending",
        "ingesting_datetime": None,
        "ingesting_status": "pending",
        "auto_collection_ids": [],
        "auto_collection_titles": [],
        "auto_collection_descriptions": [],
        "client_collection_ids": [],
        "client_collection_titles": [],
        "client_collection_descriptions": [],
    }

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]
    print("asset_properties", (request.data["asset_properties"]))
    request.data["stac_dir"] = settings.STAC_DIR

    form = InitialRecognizingForm(request.data, request.FILES)

    if form.is_valid():
        saved_form = form.save()
    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    # Making an appropriate logger_name and logger_id for the logger
    logger_name_get = (
        request.data["job_name"]
        .strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = saved_form.id

    # Sanitize the catalog_url
    catalog_url_get = request.data["catalog_url"].strip().replace("\n", "")

    try:
        manual_req["general_status"] = "Recognizing"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

        recognizer_output = Recognizer(
            main_catalog_url=catalog_url_get,
            nested_check=True,
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        recognizer_status = recognizer_output.status
        depth_number = recognizer_output.nested_num
        manual_req["depth_number_max"] = depth_number
        manual_req["general_status"] = "Recognized"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

    except Exception:
        manual_req["general_status"] = "Failed Recognizing"

        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in recognizing",
            },
            safe=False,
        )

    try:
        manual_req["general_status"] = "Processing Collections Inspector"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        nested_output = NestedCollectionInspector(
            main_catalog_url=catalog_url_get,
            nested_number=int(request.data["depth_number"]),
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        list_of_collections = list(nested_output)

        for i in range(len((list_of_collections))):
            manual_req["auto_collection_ids"].append(list_of_collections[i][1])
            manual_req["auto_collection_titles"].append(
                list_of_collections[i][2]
            )
            manual_req["auto_collection_descriptions"].append("")
            manual_req["client_collection_ids"].append("")
            manual_req["client_collection_titles"].append("")
            manual_req["client_collection_descriptions"].append("")

        if recognizer_status not in [
            "First Scenario",
            "Second Scenario",
            "Third Scenario",
            "Eighth Scenario",
            "Ninth Scenario",
        ]:
            # TODO: Analysing logs for ERRORS, and if there is any error so far, we can save the object with an error label to make the color of label red in the frontend
            manual_req["general_status"] = "Non-Nested Collection"
            APIHarvesterIngester.objects.update_or_create(
                id=saved_form.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": "success",
                    "recognizer_status": str(recognizer_status),
                    "depth_number": (depth_number),
                    "nested_output": list(nested_output),
                },
                safe=False,
            )

        else:
            manual_req["general_status"] = "Nested Collection"
            APIHarvesterIngester.objects.update_or_create(
                id=saved_form.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": "Attention: This is a nested collection, and the auto-details of collections are obtained at depth zero. To modify the depth number, simply access the Nested Collection Inspector and retrieve the details of the nested collections once more.",
                    "recognizer_status": str(recognizer_status),
                    "depth_number": (depth_number),
                    "nested_output": list(nested_output),
                },
                safe=False,
            )

    except Exception:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Collections Inspector"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

        return JsonResponse(
            {
                "message": "failed to find auto collections details",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_harvesting_recognizing(request):
    """
    This is a function for recognizing and then harvesting.
    """

    request.data._mutable = True
    print("asset_properties", request.data["asset_properties"])
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Making labels for the fields that are not filled by the user
    manual_req = {
        "created_at": datetime.now(),
        "depth_number_max": 10,
        "general_status": "pending",
        "harvesting_datetime": None,
        "harvesting_status": "pending",
        "ingesting_datetime": None,
        "ingesting_status": "pending",
        "auto_collection_ids": [],
        "auto_collection_titles": [],
        "auto_collection_descriptions": [],
        "client_collection_ids": [],
        "client_collection_titles": [],
        "client_collection_descriptions": [],
    }

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    form = HarvesterRecognizerForm(request.data, request.FILES)

    if form.is_valid():
        saved_form = form.save()
    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    # Making an appropriate logger_name and logger_id for the logger
    logger_name_get = (
        request.data["job_name"]
        .strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = saved_form.id

    # Sanitize the catalog_url
    catalog_url_get = request.data["catalog_url"].strip().replace("\n", "")

    # Making a stac_dir for the job
    stac_dir_get = (
        request.data["stac_dir"]
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    # Sanitize the datetime_filter
    if (
        request.data["dateValue"] != ""
        and request.data["dateValue"] != "null"
        and request.data["dateValue"].isspace()
    ):
        datetime_get = request.data["dateValue"].split(",")
        if len(datetime_get) != 0:
            start_datetime_format = datetime.strptime(
                datetime_get[0], "%Y-%m-%d %H:%M:%S"
            )
            end_datetime_format = datetime.strptime(
                datetime_get[1], "%Y-%m-%d %H:%M:%S"
            )
            datetime_string_get = [
                start_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                end_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            ]
    else:
        datetime_string_get = None

    # Sanitize the aggregated_url, #TODO: we have to add a validator for this field
    aggregated_url_get = (
        request.data["aggregated_url"].strip().replace("\n", "")
    )

    # Sanitize the spatial_information
    spatial_information_list = request.data["spatial_information"].split(",")

    if spatial_information_list != "" and len(spatial_information_list) == 4:
        if (
            spatial_information_list[0] == spatial_information_list[1]
            and spatial_information_list[2] == spatial_information_list[3]
        ):
            spatial_information_list = [
                spatial_information_list[0],
                spatial_information_list[2],
            ]
    else:
        spatial_information_list = None

    # Sanitize the temporal_information
    if (
        request.data["temporal_format_by_dataname"].strip().replace("\n", "")
        == ""
    ):
        temporal_information_get = None
    else:
        temporal_information_get = (
            request.data["temporal_format_by_dataname"]
            .strip()
            .replace("\n", "")
        )

    # Making a job_dir
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    os.makedirs(job_dir, exist_ok=True)
    os.makedirs(job_dir1, exist_ok=True)

    # Sanitize the asset_properties
    thumbnail_overview_url = settings.THUMBNAIL_OVERVIEW_URL

    if request.data["asset_properties"] != {} and request.data[
        "asset_properties"
    ] != {"collection_custom_asset": [], "item_custom_asset": []}:
        print(request.data["asset_properties"])
        request.data["asset_properties"] = request.data["asset_properties"]

        if (
            request.data["asset_properties"].get("collection_thumbnail_link")
            is not None
        ):
            _, file_extension = os.path.splitext(
                request.data["asset_properties"]["collection_thumbnail_link"]
            )
            request.data["asset_properties"]["collection_thumbnail_link"] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "thumbnail"
                + file_extension
            )
        if (
            request.data["asset_properties"].get("collection_overview_link")
            is not None
        ):
            _, file_extension = os.path.splitext(
                request.data["asset_properties"]["collection_overview_link"]
            )
            request.data["asset_properties"]["collection_overview_link"] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "overview"
                + file_extension
            )
    print(request.data["asset_properties"])
    # Sanitize the extension_properties
    extension_properties_dict = request.data["extension_properties"]
    extension_properties_list = []
    for key, value in extension_properties_dict.items():
        if isinstance(extension_properties_dict[key], list):
            with open(job_dir1 + "/" + key, "w") as file:
                file.write(extension_properties_dict[key][2])
                extension_properties_list.append(
                    (
                        extension_properties_dict[key][0],
                        extension_properties_dict[key][1],
                        job_dir1 + "/" + key,
                    )
                )
        if isinstance(extension_properties_dict[key], str):
            extension_properties_list.append(extension_properties_dict[key])
    extension_properties_list.append("common_metadata")
    final_extension_propertie = {
        "item_extensions": extension_properties_list,
    }

    # Sanitize the extra_metadata
    extra_metadata_file_path = os.path.join(job_dir, "extra_metadata.json")
    if request.data["extra_metadata"] != {
        "collection": {"extra_fields": {}},
        "item": {"properties": {}},
    }:
        with open(extra_metadata_file_path, "w") as file:
            json.dump(dict(request.data["extra_metadata"]), file, indent=4)
        extra_metadata_dict = {
            "extra_metadata": True,
            "extra_metadata_file": extra_metadata_file_path,
        }
    else:
        extra_metadata_dict = None
    print(request.data["extra_metadata"])
    # Sanitize the webservice_properties
    webservice_properties_dict = (
        request.data["webservice_properties"]
    ).replace("\r\n", "\r\n")
    if webservice_properties_dict != {}:
        with open(job_dir + "/tag_file.json", "w") as file:
            file.write(webservice_properties_dict)
        # json.dump(webservice_properties_dict, file, indent=16)
    else:
        webservice_properties_dict = None

    # Sanitize the requests_properties
    requests_properties_dict = request.data["requests_properties"]
    if requests_properties_dict != {}:
        if requests_properties_dict.get("auth") is not None and isinstance(
            requests_properties_dict.get("auth"), list
        ):
            requests_properties_dict["auth"] = tuple(
                requests_properties_dict["auth"]
            )

    # stac_validation_input = request.data["stac_validation_input"]
    # if stac_validation_input != {}:
    #     if stac_validation_input.get("staccatalog") is not False:
    #         print("activate a funtion to get the Catalog validation and fill the catalog_validation field in the model")
    #     if stac_validation_input.get("staccollection") is not False:
    #         print("activate a funtion to get the Collection validation and fill the collection_validation field in the model")
    #     if stac_validation_input.get("stacitem") is not False:
    #         print("activate a funtion to get the Item validation and fill the item_validation field in the model")

    try:
        manual_req["general_status"] = "Recognizing"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

        recognizer_output = Recognizer(
            main_catalog_url=catalog_url_get,
            nested_check=True,
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        recognizer_status = recognizer_output.status
        depth_number = recognizer_output.nested_num
        manual_req["depth_number_max"] = depth_number
        manual_req["general_status"] = "Recognized"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
    except Exception:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Recognizing"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in recognizing",
            },
            safe=False,
        )
    # if recognizer_status not in ["First Scenario", "Second Scenario", "Third Scenario", "Eighth Scenario", "Ninth Scenario"]:
    try:
        manual_req["general_status"] = "Processing Collections Inspector"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        nested_output = NestedCollectionInspector(
            main_catalog_url=catalog_url_get,
            nested_number=int(request.data["depth_number"]),
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        manual_req["general_status"] = "Finished Collections Inspector"
        list_of_collections = list(nested_output)

        for i in range(len((list_of_collections))):
            manual_req["auto_collection_ids"].append(list_of_collections[i][1])
            manual_req["auto_collection_titles"].append(
                list_of_collections[i][2]
            )
            manual_req["auto_collection_descriptions"].append("")
            manual_req["client_collection_ids"].append("")
            manual_req["client_collection_titles"].append("")
            manual_req["client_collection_descriptions"].append("")
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

    except Exception:
        manual_req["general_status"] = "Failed Collections Inspector"
        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed to find auto-collections details",
            },
            safe=False,
        )
    try:
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Started"
        manual_req["general_status"] = "Harvesting"

        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        TDS2STACIntegrator(
            TDS_catalog=catalog_url_get,
            stac_dir=stac_dir_get,
            stac_id=request.data["stac_id"],
            stac_title=request.data["stac_title"],
            stac_description=request.data["stac_desc"],
            # collection_tuples = merged_collection_tuples,
            datetime_filter=datetime_string_get,
            aggregated_dataset_url=aggregated_url_get,
            depth_number=int(request.data["depth_number"]),
            spatial_information=spatial_information_list,
            temporal_format_by_dataname=temporal_information_get,
            item_geometry_linestring=bool(saved_form.item_geometry_linestring),
            limited_number=5,
            extra_metadata=extra_metadata_dict,
            asset_properties=request.data["asset_properties"],
            extension_properties=final_extension_propertie,
            webservice_properties={
                "web_service_config_file": job_dir + "/tag_file.json"
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
            requests_properties=requests_properties_dict,
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
        )
        # Analysing the logs
        try:
            response = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response.status_code == 200:
                log_analysis = [
                    log.get("levelname") for log in response.json()
                ]
                if "CRITICAL" in log_analysis:
                    manual_req["general_status"] = "Failed Harvesting"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Failed"
                    APIHarvesterIngester.objects.update_or_create(
                        id=saved_form.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in harvesting",
                            "recognizer_status": str(recognizer_status),
                            "depth_number": (depth_number),
                            "nested_output": list(nested_output),
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis:
                    manual_req["general_status"] = "Harvested with Errors"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=saved_form.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "Harvested with Errors in logs",
                            "recognizer_status": str(recognizer_status),
                            "depth_number": (depth_number),
                            "nested_output": list(nested_output),
                        },
                        safe=False,
                    )
                else:
                    manual_req["general_status"] = "Harvested"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=saved_form.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "success",
                            "recognizer_status": str(recognizer_status),
                            "depth_number": (depth_number),
                            "nested_output": list(nested_output),
                        },
                        safe=False,
                    )
            response.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngester.objects.update_or_create(
                id=saved_form.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Harvesting"

        APIHarvesterIngester.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in harvesting",
            },
            safe=False,
        )


##############################################################################################################
# place of functions for Test Jobs
##############################################################################################################
@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_close_by_id(request):
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngester.objects.get(id=instance_id)
        except APIHarvesterIngester.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None
    form = SaveCloseForm(request.data, request.FILES, instance=instance)

    if form.is_valid():
        form.save()

        return JsonResponse(
            {
                "message": "success",
            },
            safe=False,
        )
    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_nestedcollection_close_by_id(request):
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngester.objects.get(id=instance_id)
        except APIHarvesterIngester.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    form = NestedCollectionForm(request.data, request.FILES, instance=instance)

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_testjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_testjob_object.id
    # Sanitize the catalog_url
    catalog_url_get = get_testjob_object.catalog_url.strip().replace("\n", "")

    manual_req = {
        "general_status": "Processing Collections Inspector",
        "auto_collection_ids": [],
        "auto_collection_titles": [],
        "auto_collection_descriptions": [],
        "client_collection_ids": [],
        "client_collection_titles": [],
        "client_collection_descriptions": [],
    }
    APIHarvesterIngester.objects.update_or_create(
        id=get_testjob_object.id, defaults=manual_req
    )

    try:
        nested_output = NestedCollectionInspector(
            main_catalog_url=catalog_url_get,
            nested_number=int(get_testjob_object.depth_number),
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        list_of_collections = list(nested_output)

        for i in range(len((list(list_of_collections)))):
            manual_req["auto_collection_ids"].append(list_of_collections[i][1])
            manual_req["auto_collection_titles"].append(
                list_of_collections[i][2]
            )
            manual_req["auto_collection_descriptions"].append("")
            manual_req["client_collection_ids"].append("")
            manual_req["client_collection_titles"].append("")
            manual_req["client_collection_descriptions"].append("")

            # TODO: Analysing logs for ERRORS, and if there is any error so far, we can save the object with an error label to make the color of label red in the frontend
        if get_testjob_object.depth_number_max > 0:
            manual_req["general_status"] = "Nested Collection"
        else:
            manual_req["general_status"] = "Non-Nested Collection"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "success",
                "nested_output": list(nested_output),
            },
            safe=False,
        )

    except Exception:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Collections Inspector"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed to find auto collections details",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_harvest_ingest_close_by_id(request):
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngester.objects.get(id=instance_id)
        except APIHarvesterIngester.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None
    depth_number_prev = instance.depth_number

    form = SaveHarvestIngestCloseForm(
        request.data, request.FILES, instance=instance
    )

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_testjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_testjob_object.id
    # Sanitize the catalog_url
    catalog_url_get = get_testjob_object.catalog_url.strip().replace("\n", "")

    # Making a stac_dir
    stac_dir_get = (
        get_testjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    # Sanitize the datetime_filter
    if (
        get_testjob_object.dateValue is not None
        and get_testjob_object.dateValue != ""
        and get_testjob_object.dateValue != "null"
        and get_testjob_object.dateValue.isspace()
    ):
        datetime_get = get_testjob_object.dateValue.split(",")
        if len(datetime_get) != 0:
            start_datetime_format = datetime.strptime(
                datetime_get[0], "%Y-%m-%d %H:%M:%S"
            )
            end_datetime_format = datetime.strptime(
                datetime_get[1], "%Y-%m-%d %H:%M:%S"
            )
            datetime_string_get = [
                start_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                end_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            ]
    else:
        datetime_string_get = None

    # Sanitize the aggregated_url, TODO: we have to add a validator for this field
    if get_testjob_object.aggregated_url is not None:
        aggregated_url_get = get_testjob_object.aggregated_url.strip().replace(
            "\n", ""
        )
    else:
        aggregated_url_get = None

    # Sanitize the spatial_information
    spatial_information_list = get_testjob_object.spatial_information
    if spatial_information_list != "" and len(spatial_information_list) == 4:
        if (
            spatial_information_list[0] == spatial_information_list[1]
            and spatial_information_list[2] == spatial_information_list[3]
        ):
            spatial_information_list = [
                spatial_information_list[0],
                spatial_information_list[2],
            ]
    else:
        spatial_information_list = None

    # Sanitize the temporal_information
    temporal_information_get = get_testjob_object.temporal_format_by_dataname
    if get_testjob_object.temporal_format_by_dataname is not None:
        if (
            get_testjob_object.temporal_format_by_dataname.strip().replace(
                "\n", ""
            )
            == ""
        ):
            temporal_information_get = None
        else:
            temporal_information_get = (
                get_testjob_object.temporal_format_by_dataname.strip().replace(
                    "\n", ""
                )
            )

    # Making a job_dir
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    os.makedirs(job_dir, exist_ok=True)
    os.makedirs(job_dir1, exist_ok=True)

    # Sanitize the asset_properties
    thumbnail_overview_url = settings.THUMBNAIL_OVERVIEW_URL
    if (
        get_testjob_object.asset_properties != {}
        and get_testjob_object.asset_properties is not None
    ):
        if (
            get_testjob_object.asset_properties.get(
                "collection_thumbnail_link"
            )
            is not None
        ):
            _, file_extension = os.path.splitext(
                get_testjob_object.asset_properties[
                    "collection_thumbnail_link"
                ]
            )
            get_testjob_object.asset_properties[
                "collection_thumbnail_link"
            ] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "thumbnail"
                + file_extension
            )
        if (
            get_testjob_object.asset_properties.get("collection_overview_link")
            is not None
        ):
            _, file_extension = os.path.splitext(
                request.data["asset_properties"]["collection_overview_link"]
            )
            get_testjob_object.asset_properties["collection_overview_link"] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "overview"
                + file_extension
            )

    # Sanitize the extension_properties
    extension_properties_dict = get_testjob_object.extension_properties
    extension_properties_list = []
    for key, value in extension_properties_dict.items():
        if isinstance(extension_properties_dict[key], list):
            with open(job_dir1 + "/" + key, "w") as file:
                file.write(extension_properties_dict[key][2])
                extension_properties_list.append(
                    (
                        extension_properties_dict[key][0],
                        extension_properties_dict[key][1],
                        job_dir1 + "/" + key,
                    )
                )
        if isinstance(extension_properties_dict[key], str):
            extension_properties_list.append(extension_properties_dict[key])
    extension_properties_list.append("common_metadata")
    final_extension_propertie = {
        "item_extensions": extension_properties_list,
    }

    # Sanitize the extra_metadata
    extra_metadata_file_path = os.path.join(job_dir, "extra_metadata.json")

    if get_testjob_object.extra_metadata != {
        "collection": {"extra_fields": {}},
        "item": {"properties": {}},
    }:
        with open(extra_metadata_file_path, "w") as file:
            json.dump(dict(get_testjob_object.extra_metadata), file, indent=4)
        extra_metadata_dict = {
            "extra_metadata": True,
            "extra_metadata_file": extra_metadata_file_path,
        }
    else:
        extra_metadata_dict = None

    # Sanitize the webservice_properties
    webservice_properties_dict = (
        get_testjob_object.webservice_properties
    ).replace("\r\n", "\r\n")
    if webservice_properties_dict != {}:
        with open(job_dir + "/tag_file.json", "w") as file:
            file.write(webservice_properties_dict)
        # json.dump(webservice_properties_dict, file, indent=16)
    else:
        webservice_properties_dict = None

    # Sanitize the requests_properties
    requests_properties_dict = get_testjob_object.requests_properties

    # stac_validation_input = get_testjob_object.stac_validation_input
    # if stac_validation_input != {}:
    #     if stac_validation_input.get("staccatalog") is not False:
    #         print("activate a funtion to get the Catalog validation and fill the catalog_validation field in the model")
    #     if stac_validation_input.get("staccollection") is not False:
    #         print("activate a funtion to get the Collection validation and fill the collection_validation field in the model")
    #     if stac_validation_input.get("stacitem") is not False:
    #         print("activate a funtion to get the Item validation and fill the item_validation field in the model")

    # Sanitize the client_collection details
    print(
        "depth_number_prev",
        depth_number_prev,
        "depth_number",
        request.data["depth_number"],
    )
    if int(depth_number_prev) == int(request.data["depth_number"]):
        manual_req = {
            "general_status": "pending",
            "harvesting_status": "peding",
            "harvesting_datetime": None,
            "ingesting_status": "pending",
            "ingesting_datetime": None,
        }
        if len(get_testjob_object.auto_collection_ids) != 0:
            if len(get_testjob_object.client_collection_ids) != 0:
                # replace the None with "" in the client_collection_ids list
                client_collection_ids_modified = [
                    "" if v is None else v
                    for v in get_testjob_object.client_collection_ids
                ]
            else:
                client_collection_ids_modified = [""]
            if len(get_testjob_object.client_collection_titles) != 0:
                # replace the None with "" in the client_collection_titles list
                client_collection_titles_modified = [
                    "" if v is None else v
                    for v in get_testjob_object.client_collection_titles
                ]
            else:
                client_collection_titles_modified = [""]
            if len(get_testjob_object.client_collection_descriptions) != 0:
                # replace the None with "" in the client_collection_descriptions list
                client_collection_descriptions_modified = [
                    "" if v is None else v
                    for v in get_testjob_object.client_collection_descriptions
                ]
            else:
                client_collection_descriptions_modified = [""]

            # merge the client_collection details with the auto_collection details

            merged_collection_tuples = list(
                zip(
                    get_testjob_object.auto_collection_ids,
                    client_collection_ids_modified,
                    client_collection_titles_modified,
                    client_collection_descriptions_modified,
                )
            )

        else:
            merged_collection_tuples = None
    else:
        manual_req = {
            "general_status": "pending",
            "harvesting_status": "pending",
            "harvesting_datetime": None,
            "ingesting_status": "pending",
            "ingesting_datetime": None,
            "auto_collection_ids": [],
            "auto_collection_titles": [],
            "auto_collection_descriptions": [],
            "client_collection_ids": [],
            "client_collection_titles": [],
            "client_collection_descriptions": [],
        }
        try:
            manual_req["general_status"] = "Processing Collections Inspector"
            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )
            nested_output = NestedCollectionInspector(
                main_catalog_url=catalog_url_get,
                nested_number=int(request.data["depth_number"]),
                logger_properties={
                    "logger_handler": "HTTPHandler",
                    "logger_id": logger_id_get,
                    "logger_name": logger_name_get,
                    "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                    "logger_handler_port": "8003",
                    "logger_handler_url": "/api/logger/logger_post/",
                    "logger_handler_method": "POST",
                },
            )
            list_of_collections = list(nested_output)
            for i in range(len((list_of_collections))):
                manual_req["auto_collection_ids"].append(
                    list_of_collections[i][1]
                )
                manual_req["auto_collection_titles"].append(
                    list_of_collections[i][2]
                )
                manual_req["auto_collection_descriptions"].append("")
                manual_req["client_collection_ids"].append("")
                manual_req["client_collection_titles"].append("")
                manual_req["client_collection_descriptions"].append("")

            merged_collection_tuples = list(
                zip(
                    manual_req["auto_collection_descriptions"],
                    manual_req["client_collection_ids"],
                    manual_req["client_collection_titles"],
                    manual_req["client_collection_descriptions"],
                )
            )
            manual_req["general_status"] = "Nested Collection"
            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )

        except Exception:
            print(traceback.format_exc())
            manual_req["general_status"] = "Failed Collections Inspector"

            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )

            return JsonResponse(
                {
                    "message": "failed to find auto collections details",
                },
                safe=False,
            )

    try:
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Started"
        manual_req["general_status"] = "Harvesting"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

        TDS2STACIntegrator(
            TDS_catalog=catalog_url_get,
            stac_dir=stac_dir_get,
            stac_id=get_testjob_object.stac_id,
            stac_title=get_testjob_object.stac_title,
            stac_description=get_testjob_object.stac_desc,
            collection_tuples=merged_collection_tuples,
            datetime_filter=datetime_string_get,
            aggregated_dataset_url=aggregated_url_get,
            depth_number=int(get_testjob_object.depth_number),
            spatial_information=spatial_information_list,
            temporal_format_by_dataname=temporal_information_get,
            item_geometry_linestring=bool(
                get_testjob_object.item_geometry_linestring
            ),
            limited_number=5,
            extra_metadata=extra_metadata_dict,
            asset_properties=get_testjob_object.asset_properties,
            extension_properties=final_extension_propertie,
            webservice_properties={
                "web_service_config_file": job_dir + "/tag_file.json"
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
            requests_properties=requests_properties_dict,
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
        )

        # Analysing the logs
        try:
            response = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response.status_code == 200:
                log_analysis = [
                    log.get("levelname") for log in response.json()
                ]
                if "CRITICAL" in log_analysis:
                    manual_req["general_status"] = "Failed Harvesting"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Failed"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in harvesting",
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis:
                    manual_req["general_status"] = "Harvested with Errors"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )

                else:
                    manual_req["general_status"] = "Harvested"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )

            response.raise_for_status()
        except requests.RequestException as logger_api_error:
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Harvesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in harvesting",
            },
            safe=False,
        )

    try:
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Started"
        manual_req["general_status"] = "Ingesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstactest",
                "POSTGRES_HOST_WRITER": "pgstactest",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstactest",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstactest",
                "PGDATABASE": "pgstactest",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_testjob_object.id,
                "logger_name": get_testjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response.status_code == 200:
                log_analysis_ingesting = [
                    log.get("levelname") for log in response.json()
                ]
                if "CRITICAL" in log_analysis_ingesting:
                    manual_req["general_status"] = "Failed Ingesting"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in ingesting",
                        },
                        safe=False,
                    )
                elif (
                    "ERROR" in log_analysis_ingesting
                    and "ERROR" not in log_analysis
                ):
                    manual_req["general_status"] = "Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                elif (
                    "ERROR" in log_analysis_ingesting
                    and "ERROR" in log_analysis
                ):
                    manual_req[
                        "general_status"
                    ] = "Harvested and Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                else:
                    manual_req["general_status"] = "Ingested"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )

            response.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Ingesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingesting",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_delete_harvest_ingest_close_by_id(request):
    request.data._mutable = True
    # Saniting the job_name

    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngester.objects.get(id=instance_id)
        except APIHarvesterIngester.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None
    depth_number_prev = instance.depth_number

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        request.data["job_name"]
        .strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = instance.id
    # Sanitize the catalog_url
    catalog_url_get = instance.catalog_url.strip().replace("\n", "")

    # Making a stac_dir
    stac_dir_get = (
        instance.stac_dir + "/" + logger_name_get + "_" + str(logger_id_get)
    )

    # Making a job_dir
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )

    try:
        manual_req = {
            "general_status": "Deleting",
            "harvesting_status": request.data.get("harvesting_status")
            if request.data.get("harvesting_status") != "null"
            else None,
            "harvesting_datetime": request.data.get("harvesting_datetime")
            if request.data.get("harvesting_datetime") != "null"
            else None,
            "ingesting_status": request.data.get("ingesting_status")
            if request.data.get("ingesting_status") != "null"
            else None,
            "ingesting_datetime": request.data.get("ingesting_datetime")
            if request.data.get("ingesting_datetime") != "null"
            else None,
        }
        APIHarvesterIngester.objects.update_or_create(
            id=instance.id, defaults=manual_req
        )
        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Delete",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstactest",
                "POSTGRES_HOST_WRITER": "pgstactest",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstactest",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstactest",
                "PGDATABASE": "pgstactest",
            },
            # logger_properties={
            #     "logger_handler": "StreamHandler",
            #     "logger_id": logger_id_get,
            #     "logger_name": logger_name_get,
            # }
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": str(logger_id_get),
                "logger_name": str(logger_name_get),
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response_deleting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            response_deleting.raise_for_status()
        except requests.RequestException:
            print(traceback.format_exc())
            manual_req["general_status"] = "Failed Deleting Logs"
            APIHarvesterIngester.objects.update_or_create(
                id=instance.id, defaults=manual_req
            )
    except Exception as stac_error:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Deleting"
        APIHarvesterIngester.objects.update_or_create(
            id=instance.id, defaults=manual_req
        )
        return JsonResponse(
            {"message": "Failed in deleting TestJobObject." + str(stac_error)},
            status=500,
            safe=False,
        )

    if manual_req["harvesting_status"] == "Finished":
        try:
            if os.path.exists(stac_dir_get):
                shutil.rmtree(stac_dir_get)
            if os.path.exists(job_dir):
                shutil.rmtree(job_dir)
            if os.path.exists(job_dir1):
                shutil.rmtree(job_dir1)

            manual_req["general_status"] = "Deleted"
            APIHarvesterIngester.objects.update_or_create(
                id=instance.id, defaults=manual_req
            )
        except Exception:
            print(traceback.format_exc())
            manual_req["general_status"] = "Deleted With Errors"
            APIHarvesterIngester.objects.update_or_create(
                id=instance.id, defaults=manual_req
            )
    else:
        print("Failed in deleting from local storage.")

    form = SaveHarvestIngestCloseForm(
        request.data, request.FILES, instance=instance
    )

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    # Sanitize the datetime_filter
    if (
        get_testjob_object.dateValue is not None
        and get_testjob_object.dateValue != ""
        and get_testjob_object.dateValue != "null"
        and get_testjob_object.dateValue.isspace()
    ):
        datetime_get = get_testjob_object.dateValue.split(",")
        if len(datetime_get) != 0:
            start_datetime_format = datetime.strptime(
                datetime_get[0], "%Y-%m-%d %H:%M:%S"
            )
            end_datetime_format = datetime.strptime(
                datetime_get[1], "%Y-%m-%d %H:%M:%S"
            )
            datetime_string_get = [
                start_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                end_datetime_format.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            ]
    else:
        datetime_string_get = None

    # Sanitize the aggregated_url, TODO: we have to add a validator for this field
    if get_testjob_object.aggregated_url is not None:
        aggregated_url_get = get_testjob_object.aggregated_url.strip().replace(
            "\n", ""
        )
    else:
        aggregated_url_get = None

    # Sanitize the spatial_information
    spatial_information_list = get_testjob_object.spatial_information
    if spatial_information_list != "" and len(spatial_information_list) == 4:
        if (
            spatial_information_list[0] == spatial_information_list[1]
            and spatial_information_list[2] == spatial_information_list[3]
        ):
            spatial_information_list = [
                spatial_information_list[0],
                spatial_information_list[2],
            ]
    else:
        spatial_information_list = None

    # Sanitize the temporal_information
    temporal_information_get = get_testjob_object.temporal_format_by_dataname
    if get_testjob_object.temporal_format_by_dataname is not None:
        if (
            get_testjob_object.temporal_format_by_dataname.strip().replace(
                "\n", ""
            )
            == ""
        ):
            temporal_information_get = None
        else:
            temporal_information_get = (
                get_testjob_object.temporal_format_by_dataname.strip().replace(
                    "\n", ""
                )
            )

    # stac_validation_input = get_testjob_object.stac_validation_input
    # if stac_validation_input != {}:
    #     if stac_validation_input.get("staccatalog") is not False:
    #         print("activate a funtion to get the Catalog validation and fill the catalog_validation field in the model")
    #     if stac_validation_input.get("staccollection") is not False:
    #         print("activate a funtion to get the Collection validation and fill the collection_validation field in the model")
    #     if stac_validation_input.get("stacitem") is not False:
    #         print("activate a funtion to get the Item validation and fill the item_validation field in the model")

    os.makedirs(job_dir, exist_ok=True)
    os.makedirs(job_dir1, exist_ok=True)
    # Sanitize the asset_properties
    thumbnail_overview_url = settings.THUMBNAIL_OVERVIEW_URL
    print(
        "get_testjob_object.asset_properties",
        get_testjob_object.asset_properties,
    )
    if (
        get_testjob_object.asset_properties != {}
        and get_testjob_object.asset_properties is not None
    ):
        if (
            get_testjob_object.asset_properties.get(
                "collection_thumbnail_link"
            )
            is not None
        ):
            _, file_extension = os.path.splitext(
                get_testjob_object.asset_properties[
                    "collection_thumbnail_link"
                ]
            )
            get_testjob_object.asset_properties[
                "collection_thumbnail_link"
            ] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "thumbnail"
                + file_extension
            )
        if (
            get_testjob_object.asset_properties.get("collection_overview_link")
            is not None
        ):
            _, file_extension = os.path.splitext(
                request.data["asset_properties"]["collection_overview_link"]
            )
            get_testjob_object.asset_properties["collection_overview_link"] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "overview"
                + file_extension
            )

    # Sanitize the extension_properties
    extension_properties_dict = get_testjob_object.extension_properties
    extension_properties_list = []
    for key, value in extension_properties_dict.items():
        if isinstance(extension_properties_dict[key], list):
            with open(job_dir1 + "/" + key, "w") as file:
                file.write(extension_properties_dict[key][2])
                extension_properties_list.append(
                    (
                        extension_properties_dict[key][0],
                        extension_properties_dict[key][1],
                        job_dir1 + "/" + key,
                    )
                )
        if isinstance(extension_properties_dict[key], str):
            extension_properties_list.append(extension_properties_dict[key])
    extension_properties_list.append("common_metadata")
    final_extension_propertie = {
        "item_extensions": extension_properties_list,
    }
    print("final_extension_propertie", final_extension_propertie)

    # Sanitize the extra_metadata
    extra_metadata_file_path = os.path.join(job_dir, "extra_metadata.json")
    print(get_testjob_object.extra_metadata)
    if get_testjob_object.extra_metadata != {
        "collection": {"extra_fields": {}},
        "item": {"properties": {}},
    }:
        with open(extra_metadata_file_path, "w") as file:
            json.dump(dict(get_testjob_object.extra_metadata), file, indent=4)
        extra_metadata_dict = {
            "extra_metadata": True,
            "extra_metadata_file": extra_metadata_file_path,
        }
    else:
        extra_metadata_dict = None

    # Sanitize the webservice_properties
    webservice_properties_dict = (
        get_testjob_object.webservice_properties
    ).replace("\r\n", "\r\n")
    if webservice_properties_dict != {}:
        with open(job_dir + "/tag_file.json", "w") as file:
            file.write(webservice_properties_dict)
        # json.dump(webservice_properties_dict, file, indent=16)
    else:
        webservice_properties_dict = None

    # Sanitize the requests_properties
    requests_properties_dict = get_testjob_object.requests_properties
    # Sanitize the client_collection details

    if int(depth_number_prev) == int(request.data["depth_number"]):
        manual_req = {
            "general_status": "pending",
            "harvesting_status": "peding",
            "harvesting_datetime": None,
            "ingesting_status": "pending",
            "ingesting_datetime": None,
        }

        if len(get_testjob_object.auto_collection_ids) != 0:
            if len(get_testjob_object.client_collection_ids) != 0:
                # replace the None with "" in the client_collection_ids list
                client_collection_ids_modified = [
                    "" if v is None else v
                    for v in get_testjob_object.client_collection_ids
                ]
            else:
                client_collection_ids_modified = [""]
            if len(get_testjob_object.client_collection_titles) != 0:
                # replace the None with "" in the client_collection_titles list
                client_collection_titles_modified = [
                    "" if v is None else v
                    for v in get_testjob_object.client_collection_titles
                ]
            else:
                client_collection_titles_modified = [""]
            if len(get_testjob_object.client_collection_descriptions) != 0:
                # replace the None with "" in the client_collection_descriptions list
                client_collection_descriptions_modified = [
                    "" if v is None else v
                    for v in get_testjob_object.client_collection_descriptions
                ]
            else:
                client_collection_descriptions_modified = [""]

            # merge the client_collection details with the auto_collection details

            merged_collection_tuples = list(
                zip(
                    get_testjob_object.auto_collection_ids,
                    client_collection_ids_modified,
                    client_collection_titles_modified,
                    client_collection_descriptions_modified,
                )
            )

        else:
            merged_collection_tuples = None

    else:
        manual_req = {
            "general_status": "pending",
            "harvesting_status": "pending",
            "harvesting_datetime": None,
            "ingesting_status": "pending",
            "ingesting_datetime": None,
            "auto_collection_ids": [],
            "auto_collection_titles": [],
            "auto_collection_descriptions": [],
            "client_collection_ids": [],
            "client_collection_titles": [],
            "client_collection_descriptions": [],
        }
        try:
            manual_req["general_status"] = "Processing Collections Inspector"
            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )
            nested_output = NestedCollectionInspector(
                main_catalog_url=catalog_url_get,
                nested_number=int(request.data["depth_number"]),
                logger_properties={
                    "logger_handler": "HTTPHandler",
                    "logger_id": logger_id_get,
                    "logger_name": logger_name_get,
                    "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                    "logger_handler_port": "8003",
                    "logger_handler_url": "/api/logger/logger_post/",
                    "logger_handler_method": "POST",
                },
            )
            list_of_collections = list(nested_output)
            for i in range(len((list_of_collections))):
                manual_req["auto_collection_ids"].append(
                    list_of_collections[i][1]
                )
                manual_req["auto_collection_titles"].append(
                    list_of_collections[i][2]
                )
                manual_req["auto_collection_descriptions"].append("")
                manual_req["client_collection_ids"].append("")
                manual_req["client_collection_titles"].append("")
                manual_req["client_collection_descriptions"].append("")

            merged_collection_tuples = list(
                zip(
                    manual_req["auto_collection_descriptions"],
                    manual_req["client_collection_ids"],
                    manual_req["client_collection_titles"],
                    manual_req["client_collection_descriptions"],
                )
            )
            manual_req["general_status"] = "Nested Collection"
            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )

        except Exception:
            print(traceback.format_exc())
            manual_req["general_status"] = "Failed Collections Inspector"

            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )

            return JsonResponse(
                {
                    "message": "failed to find auto collections details",
                },
                safe=False,
            )

    try:
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Started"
        manual_req["general_status"] = "Harvesting"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        TDS2STACIntegrator(
            TDS_catalog=catalog_url_get,
            stac_dir=stac_dir_get,
            stac_id=get_testjob_object.stac_id,
            stac_title=get_testjob_object.stac_title,
            stac_description=get_testjob_object.stac_desc,
            collection_tuples=merged_collection_tuples,
            datetime_filter=datetime_string_get,
            aggregated_dataset_url=aggregated_url_get,
            depth_number=int(get_testjob_object.depth_number),
            spatial_information=spatial_information_list,
            temporal_format_by_dataname=temporal_information_get,
            item_geometry_linestring=bool(
                get_testjob_object.item_geometry_linestring
            ),
            limited_number=5,
            extra_metadata=extra_metadata_dict,
            asset_properties=get_testjob_object.asset_properties,
            extension_properties=final_extension_propertie,
            webservice_properties={
                "web_service_config_file": job_dir + "/tag_file.json"
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
            requests_properties=requests_properties_dict,
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
        )

        # Analysing the logs
        try:
            response_harvesting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response_harvesting.status_code == 200:
                log_analysis = [
                    log.get("levelname")
                    for log in response_harvesting.json()
                    if log not in response_deleting.json()
                ]
                if "CRITICAL" in log_analysis:
                    manual_req["general_status"] = "Failed Harvesting"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Failed"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in harvesting",
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis:
                    manual_req["general_status"] = "Harvested with Errors"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )

                else:
                    manual_req["general_status"] = "Harvested"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )

            response_harvesting.raise_for_status()
        except requests.RequestException as logger_api_error:
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Harvesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in harvesting",
            },
            safe=False,
        )

    try:
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Started"
        manual_req["general_status"] = "Ingesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstactest",
                "POSTGRES_HOST_WRITER": "pgstactest",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstactest",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstactest",
                "PGDATABASE": "pgstactest",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_testjob_object.id,
                "logger_name": get_testjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response_ingesting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response_ingesting.status_code == 200:
                log_analysis_ingesting = [
                    log.get("levelname")
                    for log in response_ingesting.json()
                    if log not in response_harvesting.json()
                    and log not in response_deleting.json()
                ]
                if "CRITICAL" in log_analysis_ingesting:
                    manual_req["general_status"] = "Failed Ingesting"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in ingesting",
                        },
                        safe=False,
                    )
                elif (
                    "ERROR" in log_analysis_ingesting
                    and "ERROR" not in log_analysis
                ):
                    manual_req["general_status"] = "Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                elif (
                    "ERROR" in log_analysis_ingesting
                    and "ERROR" in log_analysis
                ):
                    manual_req[
                        "general_status"
                    ] = "Harvested and Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                else:
                    manual_req["general_status"] = "Ingested"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )

            response_ingesting.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Ingesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingesting",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_ingest_close_by_id(request):
    """
    This is a function for ingestion by id.
    """
    print("request.data", request.data["asset_properties"])
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified

    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )

    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngester.objects.get(id=instance_id)
        except APIHarvesterIngester.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    form = SaveHarvestIngestCloseForm(
        request.data, request.FILES, instance=instance
    )

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_testjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_testjob_object.id

    manual_req = {
        "ingesting_datetime": datetime.now(),
        "ingesting_status": "Started",
        "general_status": "Ingesting",
    }

    # Making a stac_dir
    stac_dir_get = (
        get_testjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    try:
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstactest",
                "POSTGRES_HOST_WRITER": "pgstactest",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstactest",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstactest",
                "PGDATABASE": "pgstactest",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_testjob_object.id,
                "logger_name": get_testjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response.status_code == 200:
                log_analysis_ingesting = [
                    log.get("levelname") for log in response.json()
                ]
                if "CRITICAL" in log_analysis_ingesting:
                    manual_req["general_status"] = "Failed Ingesting"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in ingesting",
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis_ingesting:
                    manual_req["general_status"] = "Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
                else:
                    manual_req["general_status"] = "Ingested"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngester.objects.update_or_create(
                        id=get_testjob_object.id, defaults=manual_req
                    )
            response.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )

    except Exception:
        print(traceback.format_exc())
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Ingesting"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingesting",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_delete_by_id_force(request):
    """
    This is a function for updating by id.
    """
    manual_req = {
        "general_status": "Deleting",
        "harvesting_status": request.data.get("harvesting_status")
        if request.data.get("harvesting_status") != "null"
        else None,
        "harvesting_datetime": request.data.get("harvesting_datetime")
        if request.data.get("harvesting_datetime") != "null"
        else None,
        "ingesting_status": request.data.get("ingesting_status")
        if request.data.get("ingesting_status") != "null"
        else None,
        "ingesting_datetime": request.data.get("ingesting_datetime")
        if request.data.get("ingesting_datetime") != "null"
        else None,
    }
    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    APIHarvesterIngester.objects.update_or_create(
        id=get_testjob_object.id, defaults=manual_req
    )
    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_testjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_testjob_object.id

    # Making a stac_dir
    stac_dir_get = (
        get_testjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    try:
        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Delete",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            # logger_properties={
            #     "logger_handler": "StreamHandler",
            #     "logger_id": logger_id_get,
            #     "logger_name": logger_name_get,
            # }
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_testjob_object.id,
                "logger_name": get_testjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

    except Exception:
        manual_req["general_status"] = "Force Deleting STAC"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

    try:
        response = requests.delete(
            f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_delete_object_by_id/",
            data={"name": logger_name_get + "_" + str(logger_id_get)},
        )
        response.raise_for_status()
    except requests.RequestException:
        manual_req["general_status"] = "Force Deleting Logger"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

    try:
        get_testjob_object.delete()

    except ObjectDoesNotExist:
        manual_req["general_status"] = "Force Deleting Object"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse({"message": "Object not found"}, status=404)
    except Exception:
        manual_req["general_status"] = "Force Deleting Object"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

    if manual_req["harvesting_status"] == "Finished":
        try:
            if os.path.exists(stac_dir_get):
                shutil.rmtree(stac_dir_get)
            if os.path.exists(job_dir):
                shutil.rmtree(job_dir)
            if os.path.exists(job_dir1):
                shutil.rmtree(job_dir1)
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except Exception as directory_error:
            manual_req["general_status"] = "Force Deleting Local Directory"
            APIHarvesterIngester.objects.update_or_create(
                id=get_testjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in deleting local directory: {str(directory_error)}"
                },
                status=200,
            )
    else:
        return JsonResponse(
            {"message": "success"},
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_delete_by_id(request):
    """
    This is a function for updating by id.
    """

    manual_req = {
        "general_status": "Deleting",
        "harvesting_status": request.data.get("harvesting_status")
        if request.data.get("harvesting_status") != "null"
        else None,
        "harvesting_datetime": request.data.get("harvesting_datetime")
        if request.data.get("harvesting_datetime") != "null"
        else None,
        "ingesting_status": request.data.get("ingesting_status")
        if request.data.get("ingesting_status") != "null"
        else None,
        "ingesting_datetime": request.data.get("ingesting_datetime")
        if request.data.get("ingesting_datetime") != "null"
        else None,
    }
    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    APIHarvesterIngester.objects.update_or_create(
        id=get_testjob_object.id, defaults=manual_req
    )
    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_testjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_testjob_object.id

    # Making a stac_dir
    stac_dir_get = (
        get_testjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    try:
        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Delete",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstactest",
                "POSTGRES_HOST_WRITER": "pgstactest",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstactest",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstactest",
                "PGDATABASE": "pgstactest",
            },
            # logger_properties={
            #     "logger_handler": "StreamHandler",
            #     "logger_id": logger_id_get,
            #     "logger_name": logger_name_get,
            # }
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_testjob_object.id,
                "logger_name": get_testjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

    except Exception as stac_error:
        manual_req["general_status"] = "Failed Deleting"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {"message": "Failed in deleting TestJobObject." + str(stac_error)},
            status=500,
            safe=False,
        )

    try:
        response = requests.delete(
            f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_delete_object_by_id/",
            data={"name": logger_name_get + "_" + str(logger_id_get)},
        )
        response.raise_for_status()
    except requests.RequestException as logger_api_error:
        manual_req["general_status"] = "Failed Deleting"
        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": f"Failed in deleting Logger via API: {str(logger_api_error)}"
            },
            status=500,
        )
    try:
        get_testjob_object.delete()
    except ObjectDoesNotExist:
        return JsonResponse({"message": "Object not found"}, status=404)
    except Exception as delete_error:
        JsonResponse(
            {
                "message": "failed in deleting the object from Test Job Table. "
                + str(delete_error)
            },
            status=500,
        )
    if manual_req["harvesting_status"] == "Finished":
        try:
            if os.path.exists(stac_dir_get):
                shutil.rmtree(stac_dir_get)
            if os.path.exists(job_dir):
                shutil.rmtree(job_dir)
            if os.path.exists(job_dir1):
                shutil.rmtree(job_dir1)
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except Exception as directory_error:
            return JsonResponse(
                {
                    "message": f"Failed in deleting local directory: {str(directory_error)}"
                },
                status=500,
            )
    else:
        return JsonResponse(
            {"message": "success"},
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_update_close_by_id(request):
    """
    This is a function for updating by id.
    """

    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]
    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngester.objects.get(id=instance_id)
        except APIHarvesterIngester.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    form = SaveUpdateCloseForm(request.data, request.FILES, instance=instance)

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    request.data._mutable = True
    get_testjob_object = APIHarvesterIngester.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_testjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_testjob_object.id

    manual_req = {
        "updating_datetime": None,
        "updating_status": "pending",
    }

    # Making a stac_dir
    stac_dir_get = (
        get_testjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    try:
        manual_req["updating_datetime"] = datetime.now()
        manual_req["updating_status"] = "Started"
        manual_req["general_status"] = "Updating"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstactest",
                "POSTGRES_HOST_WRITER": "pgstactest",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstactest",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstactest",
                "PGDATABASE": "pgstactest",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_testjob_object.id,
                "logger_name": get_testjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        manual_req["updating_datetime"] = datetime.now()
        manual_req["updating_status"] = "Finished"
        manual_req["general_status"] = "Up-to-Date"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )

        return JsonResponse(
            {
                "message": "success",
            },
            safe=False,
        )

    except Exception:
        manual_req["updating_datetime"] = datetime.now()
        manual_req["updating_status"] = "Failed"
        manual_req["general_status"] = "Failed Updating"

        APIHarvesterIngester.objects.update_or_create(
            id=get_testjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingestion",
            },
            safe=False,
        )


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_testjob_objects(request, email: str):
    """
    Retrieve a list of test job objects filtered by email.

    This view returns all test job objects if the provided email is
    hadizadeh@dkrz.de, otherwise, it returns objects associated
    with the given email. The function performs basic email validation
    and utilizes Django's get_list_or_404 for handling empty querysets.

    Parameters:
    - request (HttpRequest): The HttpRequest object.
    - email (str): The email address to filter the test job objects.

    Returns:
    - JsonResponse: A list of serialized test job objects or an error message.

    Raises:
    - HTTP 400: If the email is invalid.
    - HTTP 404: If no test job objects are found for the given email.
    - HTTP 500: If an internal server error occurs.

    Example:
    GET /api/get_testjob_objects?email=user@example.com
    """

    try:
        if not email or "@" not in email:  # Basic validation for email
            return JsonResponse(
                {"error": "Invalid email provided"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if email != "hadizadeh@dkrz.de":
            testjob_objects = APIHarvesterIngester.objects.filter(
                email=email
            ).order_by("-created_at")
        else:
            testjob_objects = APIHarvesterIngester.objects.all().order_by(
                "-created_at"
            )

        # Using get_list_or_404 to handle empty querysets
        # testjob_objects_list = get_list_or_404(testjob_objects)
        serializer = APIHarvesterIngesterSelectedSerializer(
            testjob_objects, many=True
        )
        return JsonResponse(serializer.data, safe=False)

    except Exception:
        print(traceback.format_exc())
        return JsonResponse(
            {"error": "An error occurred while fetching data"},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_testjob_objects_by_id(request, email, id):
    print(email, id)
    get_testjob_objects = APIHarvesterIngester.objects.filter(
        email=email, id=id
    ).order_by("-created_at")
    serializer = APIHarvesterIngesterSerializer(get_testjob_objects, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def tds2stac_get_depth_client_by_id(request, email, id):
    get_testjob_objects = APIHarvesterIngester.objects.filter(
        email=email, id=id
    ).order_by("-created_at")
    serializer = APIHarvesterIngesterDepthClientSerializer(
        get_testjob_objects, many=True
    )
    return JsonResponse(serializer.data, safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def get_all_available_webservices(request):
    """Get all available webservices from the catalog."""
    catalog_url = request.data["catalog_url"].strip().replace("\n", "")
    try:
        webservices = WebServiceListScraper(
            url=catalog_url,
        )
        print(list(webservices))
        return JsonResponse(
            {
                "message": "success",
                "webservices": list(webservices),
            },
        )
    except Exception:
        print(traceback.format_exc())
        return JsonResponse(
            {
                "message": "failed in getting available webservices",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_validate_catalog_url(request):
    """Get all available webservices from the catalog."""
    catalog_url = request.data["catalog_url"].strip().replace("\n", "")
    if catalog_url.isspace() or catalog_url == "":
        return JsonResponse(
            {"TDS_status": "TDS Checker msg: No catalog URL provided."}
        )
    else:
        try:
            xml_url = requests.get(
                catalog_url,
                # None,
                # auth=requests_properties["auth"],
                verify=False,
                # timeout=requests_properties["timeout"],
            )
            if xml_url.status_code == 200:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is accessible.",
                    },
                )
            else:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                    },
                    safe=False,
                )
        except BaseException:
            return JsonResponse(
                {
                    "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                },
                safe=False,
            )
