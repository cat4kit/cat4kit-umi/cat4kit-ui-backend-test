# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import path

from . import api

urlpatterns = [
    # path("tds2stac_harvesting_ingesting/", api.tds2stac_harvesting_ingesting, name="tds2stac_harvesting_ingesting"),
    path(
        "get_all_available_webservices/",
        api.get_all_available_webservices,
        name="get_all_available_webservices",
    ),
    path(
        "tds2stac_validate_catalog_url/",
        api.tds2stac_validate_catalog_url,
        name="tds2stac_validate_catalog_url",
    ),
    path(
        "get_testjob_objects/<str:email>/",
        api.get_testjob_objects,
        name="get_testjob_objects",
    ),
    path(
        "get_testjob_objects_by_id/<str:email>/<str:id>/",
        api.get_testjob_objects_by_id,
        name="get_testjob_objects_by_id",
    ),
    path(
        "tds2stac_get_depth_client_by_id/<str:email>/<str:id>/",
        api.tds2stac_get_depth_client_by_id,
        name="tds2stac_get_depth_client_by_id",
    ),
    path(
        "tds2stac_recognizing_initial_trigger/",
        api.tds2stac_recognizing_initial_trigger,
        name="tds2stac_recognizing_initial_trigger",
    ),
    path(
        "tds2stac_harvesting_recognizing/",
        api.tds2stac_harvesting_recognizing,
        name="tds2stac_harvesting_recognizing",
    ),
    path(
        "tds2stac_save_close_by_id/",
        api.tds2stac_save_close_by_id,
        name="tds2stac_save_close_by_id",
    ),
    path(
        "tds2stac_save_harvest_ingest_close_by_id/",
        api.tds2stac_save_harvest_ingest_close_by_id,
        name="tds2stac_save_harvest_ingest_close_by_id",
    ),
    path(
        "tds2stac_save_delete_harvest_ingest_close_by_id/",
        api.tds2stac_save_delete_harvest_ingest_close_by_id,
        name="tds2stac_save_delete_harvest_ingest_close_by_id",
    ),
    path(
        "tds2stac_save_nestedcollection_close_by_id/",
        api.tds2stac_save_nestedcollection_close_by_id,
        name="tds2stac_save_nestedcollection_close_by_id",
    ),
    # path("tds2stac_harvesting_by_id/", api.tds2stac_harvesting_by_id, name="tds2stac_harvesting_by_id"),
    path(
        "tds2stac_save_ingest_close_by_id/",
        api.tds2stac_save_ingest_close_by_id,
        name="tds2stac_save_ingest_close_by_id",
    ),
    path(
        "tds2stac_save_update_close_by_id/",
        api.tds2stac_save_update_close_by_id,
        name="tds2stac_save_update_close_by_id",
    ),
    path(
        "tds2stac_delete_by_id/",
        api.tds2stac_delete_by_id,
        name="tds2stac_delete_by_id",
    ),
    path(
        "tds2stac_delete_by_id_force/",
        api.tds2stac_delete_by_id_force,
        name="tds2stac_delete_by_id_force",
    ),
    # path("job_pid_killer/", api.job_pid_killer, name="job_pid_killer"),
    # path(
    #     "get_collections_logger/<str:thing>/",
    #     api.get_collections_logger,
    #     name="get_collections_logger",
    # ),
    # path(
    #     "get_collections_ids/",
    #     api.get_collections_ids,
    #     name="get_collections_ids",
    # ),
    # path(
    #     "delete_collections_logger/<str:thing>/",
    #     api.delete_collections_logger,
    #     name="delete_collections_logger",
    # ),
    # path(
    #     "post_collections_logger/",
    #     api.post_collections_logger,
    #     name="post_collections_logger",
    # ),
]
