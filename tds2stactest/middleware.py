# SPDX-FileCopyrightText: 2023 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0
from django.http import HttpResponseForbidden
from django.utils.timezone import now


class MyCustomMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # You can check for certain conditions here
        # For example, let's say you want to block a specific user agent
        if "BadBot" in request.META.get("HTTP_USER_AGENT", ""):
            return HttpResponseForbidden("Access denied")

        start_time = now()
        print(
            f"Request started at {start_time}: {request.method} {request.path}"
        )

        response = self.get_response(request)

        # Log the end of the request processing
        end_time = now()
        print(f"Request ended at {end_time}: {request.method} {request.path}")

        # If the condition is not met, just call the next middleware/view
        response = self.get_response(request)
        return response
