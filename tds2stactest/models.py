# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import os
import uuid

from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models


def get_upload_path_thumbnail(instance, filename):
    # You can modify this function to determine the path based on instance attributes
    base_dir = settings.STAC_FILES
    _, file_extension = os.path.splitext(filename)
    instance.email = instance.email.replace("@", "-").replace(".", "-")
    instance.job_name = (
        instance.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    job_dir = os.path.join(
        base_dir, f"{instance.job_name}-{instance.id}-{instance.email}"
    )
    os.makedirs(job_dir, exist_ok=True)

    return os.path.join(
        f"files/{instance.job_name}-{instance.id}-{instance.email}/",
        "thumbnail" + file_extension,
    )


def get_upload_path_overview(instance, filename):
    # You can modify this function to determine the path based on instance attributes
    base_dir = settings.STAC_FILES
    _, file_extension = os.path.splitext(filename)
    instance.email = instance.email.replace("@", "-").replace(".", "-")
    instance.job_name = (
        instance.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    job_dir = os.path.join(
        base_dir, f"{instance.job_name}-{instance.id}-{instance.email}"
    )
    os.makedirs(job_dir, exist_ok=True)

    return os.path.join(
        f"files/{instance.job_name}-{instance.id}-{instance.email}/",
        "overview" + file_extension,
    )


# Create your models here.
class APIHarvesterIngester(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    engine = "TDS2STAC"
    job_name = models.CharField(max_length=500, blank=False, null=False)
    email = models.EmailField(max_length=500, blank=False, null=False)
    catalog_url = models.CharField(max_length=500, blank=False, null=False)
    stac_dir = models.CharField(max_length=500, blank=False, null=False)
    stac_id = models.CharField(max_length=500, blank=True, null=True)
    stac_title = models.CharField(max_length=500, blank=True, null=True)
    ischanged = models.BooleanField(default=False)
    depth_number = models.IntegerField(blank=True, null=True)
    depth_number_max = models.IntegerField(blank=True, null=True)
    stac_desc = models.CharField(max_length=500, blank=True, null=True)
    dateValue = models.CharField(max_length=500, blank=True, null=True)
    spatial_information = ArrayField(
        models.FloatField(max_length=500000, blank=True, null=True),
        blank=True,
        null=True,
    )
    temporal_format_by_dataname = models.CharField(
        max_length=500, blank=True, null=True
    )
    item_geometry_linestring = models.BooleanField(default=False)
    extra_metadata = models.JSONField(blank=True, null=True)
    collection_extra_field_list = models.JSONField(blank=True, null=True)
    item_properties_list = models.JSONField(blank=True, null=True)
    collection_custom_asset_list = models.JSONField(blank=True, null=True)
    item_custom_asset_list = models.JSONField(blank=True, null=True)
    asset_properties = models.JSONField(blank=True, null=True)
    extension_properties = models.JSONField(blank=True, null=True)
    item_custom_extension_list = models.JSONField(blank=True, null=True)
    datacube_extension = models.BooleanField(default=False)
    webservice_properties = models.TextField(blank=True, null=True)
    # stac_validation_input = models.JSONField(blank=True, null=True)
    requests_properties = models.JSONField(blank=True, null=True)
    thumbnail_img = models.ImageField(
        upload_to=get_upload_path_thumbnail,
        blank=True,
        null=True,
        max_length=5000,
    )
    overview_img = models.ImageField(
        upload_to=get_upload_path_overview,
        blank=True,
        null=True,
        max_length=5000,
    )

    aggregated_url = models.CharField(max_length=500, blank=True, null=True)

    # catalog_validation = models.BooleanField(default=False)
    # collection_validation = models.BooleanField(default=False)
    # item_validation = models.BooleanField(default=False)

    auto_collection_ids = ArrayField(
        models.CharField(max_length=500, blank=True, null=True),
        blank=True,
        null=True,
    )
    auto_collection_titles = ArrayField(
        models.CharField(max_length=5000, blank=True, null=True),
        blank=True,
        null=True,
    )
    auto_collection_descriptions = ArrayField(
        models.CharField(max_length=50000, blank=True, null=True),
        blank=True,
        null=True,
    )
    client_collection_ids = ArrayField(
        models.CharField(max_length=500, blank=True, null=True),
        blank=True,
        null=True,
    )
    client_collection_titles = ArrayField(
        models.CharField(max_length=5000, blank=True, null=True),
        blank=True,
        null=True,
    )
    client_collection_descriptions = ArrayField(
        models.CharField(max_length=50000, blank=True, null=True),
        blank=True,
        null=True,
    )

    created_at = models.DateTimeField(
        blank=True, null=True
    )  # logger datetimes are stored as strings
    harvesting_datetime = models.DateTimeField(blank=True, null=True)
    ingesting_datetime = models.DateTimeField(blank=True, null=True)
    harvesting_status = models.CharField(max_length=500, blank=True, null=True)
    ingesting_status = models.CharField(max_length=500, blank=True, null=True)
    general_status = models.CharField(max_length=500, blank=True, null=True)

    def save(self, *args, **kwargs):
        # If an image already exists, delete it to ensure only one image is saved
        try:
            thumbnail_old_file = APIHarvesterIngester.objects.get(
                id=self.id
            ).thumbnail_img
            overview_old_file = APIHarvesterIngester.objects.get(
                id=self.id
            ).overview_img
            if (
                thumbnail_old_file
                and self.thumbnail_img
                and thumbnail_old_file != self.thumbnail_img
            ):
                if os.path.isfile(thumbnail_old_file.path):
                    os.remove(thumbnail_old_file.path)
            if (
                overview_old_file
                and self.overview_img
                and overview_old_file != self.overview_img
            ):
                if os.path.isfile(overview_old_file.path):
                    os.remove(overview_old_file.path)
        except APIHarvesterIngester.DoesNotExist:
            pass

        super(APIHarvesterIngester, self).save(*args, **kwargs)
