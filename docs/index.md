<!--
SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
cat4kit-ui-backend-test documentation master file
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.
-->

# Welcome to cat4kit-ui-backend-test's documentation!

[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test/-/graphs/main/charts)
<!-- TODO: uncomment the following line when the package is registered at https://readthedocs.org -->
<!-- [![Docs](https://readthedocs.org/projects/cat4kit-ui-backend-test/badge/?version=latest)](https://cat4kit-ui-backend-test.readthedocs.io/en/latest/) -->
[![Latest Release](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test/-/badges/release.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test)
<!-- TODO: uncomment the following line when the package is published at https://pypi.org -->
<!-- [![PyPI version](https://img.shields.io/pypi/v/cat4kit-ui-backend-test.svg)](https://pypi.python.org/pypi/cat4kit-ui-backend-test/) -->
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
<!-- TODO: uncomment the following line when the package is registered at https://api.reuse.software -->
<!-- [![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test)](https://api.reuse.software/info/codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test) -->

**The Cat4KIT UI Backend Test is a REST API for pre-production testing of Cat4KIT-UMI, focused on STAC-Metadata functionality and reliability before its production deployment.**

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of cat4kit-ui-backend-test! Stay tuned for
updates and discuss with us at <https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-test>
```

```{toctree}
---
maxdepth: 2
caption: "Contents:"
---
installation
configuration
api
contributing
```

## How to cite this software

```{eval-rst}
.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff
```

# License information

Copyright © 2023 KIT - Karlsruher Institut für Technologie

The source code of cat4kit-ui-backend-test is licensed under EUPL-1.2.

If not stated otherwise, the contents of this documentation is licensed
under CC-BY-4.0.

## Indices and tables

-   {ref}`genindex`
-   {ref}`modindex`
-   {ref}`search`
